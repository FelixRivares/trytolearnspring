package hello.spring_boot;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeController {

    @RequestMapping("/kek")
    public String index() {
        return "Goodbye, it's me!";
    }
}
