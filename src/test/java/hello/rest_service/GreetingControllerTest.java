package hello.rest_service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GreetingControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void checkRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/greeting?name=Jopa").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").value("Hello, Jopa!"));
              //  .andExpect(content().json("{\"id\":1,\"content\":\"Hello, Jopa!\"}"));
              //  .andExpect(content().string(equalTo("{\"id\":1,\"content\":\"Hello, Jopa!\"}")));
    }

    @Test
    public void checkRequestCount() throws Exception {
        for (int i = 1; i < 3; i++) {
            mvc.perform(MockMvcRequestBuilders.get("/greeting?name=Jopa").accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(i));
        }

    }

}
